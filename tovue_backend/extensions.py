"""
Application Extenisons.

Initialize but not register the application extensions
"""

import datetime as dt
from enum import Enum
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from sqlalchemy import Column, Integer, DateTime

db = SQLAlchemy(app=None)
migrate = Migrate(app=None)
cors = CORS(app=None, supports_credentials=True)


class BaseModel:
    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, nullable=False, default=dt.datetime.utcnow)

    @property
    def dict(self):
        columns = [column for column in self.__table__.columns]
        dictianory = {}
        for column in columns:
            is_private = column.info.get('private')
            if is_private:
                continue
            value = getattr(self, column.name)
            if isinstance(value, (dt.datetime, dt.date)):
                value = f'{value}'
            elif isinstance(value, Enum):
                value = {'text': value.name, 'value': value.value}
            dictianory[column.name] = value
        return dictianory


__all__ = ['db']
