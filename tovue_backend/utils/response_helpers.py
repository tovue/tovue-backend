"""Response helper methods."""

def get_pagination_meta_data(query_object):
    """Generate pagination metda data."""
    return {
        'current_page': query_object.page,
        'next_page': query_object.next_num,
        'prev_page': query_object.prev_num,
        'total': query_object.total
    }