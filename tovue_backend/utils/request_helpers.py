"""Request helper methods."""

from typing import List
from functools import wraps
import json

from flask import request, abort, Response


def get_argument_processor(**kwargs) -> dict:
    """Process request.args arguments with the given kwargs."""
    obj = {}
    for key in kwargs:
        if kwargs[key] == List[int]:
            obj[key] = request.args.getlist(key, type=int)
        elif kwargs[key] == List[str]:
            obj[key] = request.args.getlist(key, type=str)
        else:
            obj[key] = request.args.get(key, type=kwargs[key])
    return obj


def get_pagination_args():
    """Process request.args and get the 'page' and 'per_page' values."""
    page = request.args.get('page', type=int, default=1)
    per_page = request.args.get('per_page', type=int, default=10)
    return page, per_page


def json_only(f):
    """Force request to be JSON or return an error."""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.is_json:
            body = json.dumps({'message': 'Only JSON requests are allowed.'})
            abort(Response(response=body,
                           status=400,
                           content_type='application/json'))
        return f(*args, **kwargs)
    return decorated_function
