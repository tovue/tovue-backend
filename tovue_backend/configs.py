"""Application config objects."""

import os


class Base:
    """Base config class."""

    ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
    DATABASE_PATH = os.path.join(ROOT_PATH, os.getenv('DB_PATH', 'database.sqlite'))
    SQLALCHEMY_DATABASE_URI = f'sqlite:////{DATABASE_PATH}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TOKEN = os.getenv('TOKEN', 'Hey Jude')


class Development(Base):
    """Development config class."""
    
    DEBUG = True


class Production(Base):
    """Production config class."""
    
    DEBUG = False


def get_config(config_name: str) -> object:
    """Get config by name."""
    return {'base': Base,
            'development': Development,
            'production': Production}.get(config_name)
