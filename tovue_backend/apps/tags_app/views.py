"""Tags blueprint's views."""

from flask import request

from . import tags
from .query_handler import get_tags_query, add_tag_query, get_tag_query, modify_tag_query, delete_tag_query

from tovue_backend.utils import get_argument_processor, get_pagination_args, json_only


@tags.route('/', methods=['GET'])
def get_tags():
    """Get tags view."""
    args = get_argument_processor(name=str, description=str)
    page, per_page = get_pagination_args()
    response = get_tags_query(page, per_page, args)
    return response or {}, 200


@tags.route('/', methods=['POST'])
@json_only
def add_tag():
    """Add tag view."""
    args = request.get_json()
    response = add_tag_query(args)
    return response or {}, 201


@tags.route('/<int:id>/', methods=['GET'])
def get_tag(id):
    """Get a tag view."""
    response = get_tag_query(id)
    return response or {}, 200


@tags.route('/<int:id>/', methods=['PUT'])
@json_only
def modify_tag(id):
    """Modify a tag view."""
    args = request.get_json()
    response = modify_tag_query(id, args)
    return response or {}, 204


@tags.route('/<int:id>/', methods=['DELETE'])
def delete_tag(id):
    """Deleta a tag view."""
    response = delete_tag_query(id)
    return response or {}, 204
