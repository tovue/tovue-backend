"""Tags blueprint.

This blueprint does everything related to the tags itself.
"""
from flask import Blueprint

tags = Blueprint('tags', __name__, url_prefix='/tags/')

from . import views