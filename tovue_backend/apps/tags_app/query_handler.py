"""Tags blueprint's queries."""

from flask import abort
from sqlalchemy.exc import IntegrityError

from .models import Tag
from tovue_backend.utils import get_pagination_meta_data
from tovue_backend.extensions import db


def get_tags_query(page: int, per_page: int, args: dict) -> dict:
    """Get tags query with specified arguments."""
    name_filter = Tag.name.ilike(f'%{args["name"]}%') if args.get('name') is not None else True
    description_filter = Tag.description.ilike(f'%{args["description"]}%') if args.get('description') is not None else True
    
    query = Tag.query.filter(name_filter, description_filter).paginate(page, per_page)

    meta = get_pagination_meta_data(query)
    tags = [tag.dict for tag in query.items]

    return {'meta': meta, 'tags': tags}


def add_tag_query(args: dict) -> dict:
    """Add a new tag query."""
    new_tag = Tag()
    new_tag.name = args.get('name') or abort(400)
    new_tag.description = args.get('description')

    try:
        db.session.add(new_tag)
        db.session.commit()
    except IntegrityError:
        abort(400)

    return new_tag.dict


def get_tag_query(id: int) -> dict:
    """Get a tag by it's ID."""
    tag = Tag.query.get(id)
    if not tag:
        abort(404)

    return tag.dict


def modify_tag_query(id: int, args: dict) -> None:
    """Modify a tag by it's ID."""
    tag = Tag.query.get(id)
    if not tag:
        abort(404)

    for key, value in args.items():
        if hasattr(tag, key):
            setattr(tag, key, value)
    
    db.session.commit()
    return


def delete_tag_query(id: int) -> None:
    """Delete a tag by it's ID."""
    tag = Tag.query.get(id)
    if not tag:
        abort(404)
    
    db.session.delete(tag)
    db.session.commit()
    return
