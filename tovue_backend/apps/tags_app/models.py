"""Tags blueprint's models."""

from sqlalchemy import Column, String, Text, Integer, ForeignKey

from tovue_backend.extensions import db, BaseModel


toviews_tags = db.Table('toviews_tags', db.metadata,
                        Column('toview_id', Integer, ForeignKey('to_view.id'), primary_key=True),
                        Column('tag_id', Integer, ForeignKey('tag.id'), primary_key=True),)


class Tag(BaseModel, db.Model):
    """Tag model."""

    name = Column(String(256), nullable=False, unique=True)
    description = Column(Text(), nullable=True, unique=False)
    to_views = db.relationship('ToView', secondary=toviews_tags, backref='tags', lazy='dynamic')



