"""Views list blueprint.

This blueprint does everything related to the views itself.
"""
from flask import Blueprint

to_views = Blueprint('views', __name__, url_prefix='/views/')

from . import views