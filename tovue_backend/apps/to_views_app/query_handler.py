"""Views list blueprint's queries."""

from flask import abort
from sqlalchemy.exc import IntegrityError

from .models import ToView, MaterialType
from tovue_backend.utils import get_pagination_meta_data
from tovue_backend.extensions import db
from tovue_backend.apps.tags_app.models import Tag


def get_toviews_query(page: int, per_page: int, args: dict) -> dict:
    """Get toviews query with the specified arguments."""
    name_filter = ToView.name.ilike(f'%{args["name"]}%') if args.get('name') is not None else True
    description_filter = ToView.description.ilike(f'%{args["description"]}%') if args.get('description') is not None else True
    status_filter = ToView.done == args['status'] if args.get('status') is not None else True
    try:
        material_type_filter = ToView.material_type == MaterialType(args['material_type']) if args.get('material_type') is not None else True
    except ValueError:
        material_type_filter = True


    query = ToView.query.filter(name_filter, description_filter, status_filter, material_type_filter).paginate(page, per_page)

    meta = get_pagination_meta_data(query)
    to_views = [to_view.dict for to_view in query.items]

    return {'meta': meta, 'to_views': to_views}


def add_toview_query(args: dict) -> dict:
    """Create a new to view."""
    new_to_view = ToView()
    new_to_view.name = args.get('name')
    new_to_view.description = args.get('description')
    
    if args.get('tags') is not None:
        for _ in args.get('tags'):
            tag = Tag.query.get(_)
            if not tag:
                abort(400)
            new_to_view.tags.append(tag)

    try:
        new_to_view.material_type = MaterialType(args.get('material_type', 4))
    except ValueError:
        abort(400)

    try:
        db.session.add(new_to_view)
        db.session.commit()
    except IntegrityError:
        abort(400)

    return new_to_view.dict


def get_toview_query(id: int) -> dict:
    """Get a single To View object."""
    to_view = ToView.query.get(id)
    if not to_view:
        abort(404)

    return to_view.dict


def change_toview_status_query(id: int) -> None:
    """
    Change to view status.
    
    Change to view status from current status
    to the negate of the current status.
    """
    to_view = ToView.query.get(id)
    if not to_view:
        abort(404)

    to_view.done = not to_view.done
    db.session.commit()

    return


def modify_toview_query(id: int, args: dict) -> None:
    """Modify a to view object."""
    to_view = ToView.query.get(id)
    if not to_view:
        abort(404)

    to_view.name = args['name'] if 'name' in args else to_view.name
    to_view.description = args['description'] if 'description' in args else to_view.description
    to_view.done = args['done'] if 'done' in args else to_view.done

    if args.get('tags') is not None:
        new_tags_array = []
        for _ in args['tags']:
            tag = Tag.query.get(_)
            if not tag:
                abort(404)
            new_tags_array.append(tag)
        to_view.tags = new_tags_array

    try:
        to_view.material_type = MaterialType(args['material_type']) if 'material_type' in args else to_view.material_type
    except ValueError:
        abort(400)

    db.session.commit()
    return


def delete_toview_query(id: int) -> None:
    """Delete a to view object."""
    to_view = ToView.query.get(id)
    if not to_view:
        abort(404)

    db.session.delete(to_view)
    db.session.commit()
    
    return
