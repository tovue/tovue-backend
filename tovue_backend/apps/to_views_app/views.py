"""Views list blueprint's views."""

from typing import List

from flask import request

from tovue_backend.utils import (get_argument_processor, get_pagination_args,
                                 json_only)

from . import to_views
from .query_handler import (add_toview_query, change_toview_status_query,
                            delete_toview_query, get_toview_query,
                            get_toviews_query, modify_toview_query)


@to_views.route('/', methods=['GET'])
def get_toviews():
    """Get to views view."""
    args = get_argument_processor(tags=List[int], name=str, description=str, status=int, material_type=int)
    page, per_page = get_pagination_args()
    response = get_toviews_query(page, per_page, args)
    return response or {}, 200


@to_views.route('/', methods=['POST'])
@json_only
def add_toview():
    """Add to view view."""
    args = request.get_json()
    response = add_toview_query(args)
    return response or {}, 201


@to_views.route('/<int:id>/', methods=['GET'])
def get_toview(id):
    """Get a single to view view."""
    response = get_toview_query(id)
    return response or {}, 200


@to_views.route('/<int:id>/', methods=['PATCH'])
def change_toview_status(id):
    """Change to view status view."""
    response = change_toview_status_query(id)
    return response or {}, 204


@to_views.route('/<int:id>/', methods=['PUT'])
@json_only
def modify_toview(id):
    """Modify a to view view."""
    args = request.get_json()
    response = modify_toview_query(id, args)
    return response or {}, 204


@to_views.route('/<int:id>/', methods=['DELETE'])
def delete_toview(id):
    """Delete a to view view."""
    response = delete_toview_query(id)
    return response or {}, 204
