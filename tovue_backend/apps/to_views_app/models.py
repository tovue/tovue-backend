"""Views list blueprint's models."""

from enum import Enum as EnumClass

from sqlalchemy import Column, String, Text, Boolean, Enum

from tovue_backend.extensions import db, BaseModel


class MaterialType(EnumClass):
    """Material Types Enum."""

    watchable_material = 1
    readable_material = 2
    listanable_material = 3
    other_materials = 4


class ToView(BaseModel, db.Model):
    """ToView Model."""

    name = Column(String(256), nullable=False, unique=False)
    description = Column(Text(), nullable=True, unique=False)
    done = Column(Boolean, nullable=False, unique=False, default=False)
    material_type = Column(Enum(MaterialType), nullable=False, unique=False, default=MaterialType.other_materials)

    @property
    def dict(self):
        """JSON Serialized verion of object's data."""
        d = super().dict
        d['tags'] = [tag.dict for tag in self.tags]
        return d
