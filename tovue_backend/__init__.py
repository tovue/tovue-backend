"""Application module."""

from flask import Flask


def application_factory(config_class: object) -> Flask:
    """Flask Application Factory.
    
    Create an app with given config.
    """
    app = Flask(__name__)

    _register_configs(app=app, config_class=config_class)
    _register_extensions(app=app)
    _register_blueprints(app=app)
    _register_hooks(app=app)

    return app


def _register_configs(app: Flask, config_class: object) -> None:
    app.config.from_object(config_class)


def _register_extensions(app: Flask) -> None:
    from .extensions import db, migrate, cors

    db.init_app(app=app)
    migrate.init_app(app=app, db=db)
    cors.init_app(app=app)


def _register_blueprints(app: Flask) -> None:
    from .apps.to_views_app import to_views
    from .apps.tags_app import tags

    app.register_blueprint(to_views)
    app.register_blueprint(tags)


def _register_hooks(app: Flask) -> None:
    from flask import request, abort
    from flask.app import HTTPException
    from .extensions import db

    @app.before_request
    def before_request_hooks():
        if request.method != 'OPTIONS':
            authorization_token = request.headers.get('Authorization')
            if authorization_token != app.config['TOKEN']:
                abort(403)


    @app.errorhandler(HTTPException)
    def error_handler_hook(e):
        db.session.rollback()
        return e
