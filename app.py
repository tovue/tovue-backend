"""Application's main file."""

import os
from tovue_backend.configs import get_config
from tovue_backend import application_factory


app = application_factory(get_config(os.getenv('CONFIG')))
